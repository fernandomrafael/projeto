package br.com.address.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.address.model.Mapa;
import br.com.address.model.Rota;

public class TesteDijkstra {
	
	List<Rota> menorCaminho = new ArrayList<Rota>();

	Rota verticeCaminho = new Rota();

	Rota atual = new Rota();

	Rota vizinho = new Rota();

	List<Rota> naoVisitados = new ArrayList<Rota>();
	

public static void main(String args[]){
	
		Rota rota1 = new Rota();
		Rota rota2 = new Rota();
		Rota rota3 = new Rota();
		Rota rota4 = new Rota();
		Rota rota5 = new Rota();
		Rota rota6 = new Rota();
		Rota rota7 = new Rota();
		
		rota1.setOrigem("A"); rota1.setDestino("B"); rota1.setDistancia(10.00);
		rota2.setOrigem("B"); rota2.setDestino("D"); rota2.setDistancia(15.00);
		rota3.setOrigem("A"); rota3.setDestino("C"); rota3.setDistancia(20.00);
		rota4.setOrigem("C"); rota4.setDestino("D"); rota4.setDistancia(30.00);
		rota5.setOrigem("B"); rota5.setDestino("E"); rota5.setDistancia(50.00);
		rota6.setOrigem("D"); rota6.setDestino("E"); rota6.setDistancia(30.00);
		rota7.setOrigem("E"); rota7.setDestino("C"); rota7.setDistancia(15.00);
		
		List<Rota> listaRotas = new ArrayList<Rota>();
		
		listaRotas.add(rota1);
		listaRotas.add(rota2);
		listaRotas.add(rota3);
		listaRotas.add(rota4);
		listaRotas.add(rota5);
		listaRotas.add(rota6);
		listaRotas.add(rota7);
		
		Mapa mapa = new Mapa();
		
		mapa.setRota(listaRotas);
		
		String origem = "D";
		
		String destino = "A";
		
		Double autonomia = 10.00;
		
		Double valorLitro = 2.5;
		
		List<Rota> rotasIniciais = new ArrayList<Rota>();
		
		List<Rota> rotasDestinos = new ArrayList<Rota>();
		
		TesteDijkstra teste = new TesteDijkstra();
		
		Rota rotaFinal = new Rota();
		
		Rota rotaControle = new Rota();
		
		for (int i = 0; i < listaRotas.size(); i++) {
			
			if (origem.equals(listaRotas.get(i).getOrigem()) && destino.equals(listaRotas.get(i).getDestino())) {
				
				rotasIniciais.add(listaRotas.get(i));
				
				rotaFinal = teste.menorRotaDestino(rotasIniciais);
				
				System.out.println(rotaFinal.getOrigem() + " " +rotaFinal.getDestino() + " "+ rotaFinal.getDistancia());
				
			}else if(rotaFinal.getDestino() == null){
					
				if (origem.equals(listaRotas.get(i).getOrigem())) {
						
					rotasIniciais.add(listaRotas.get(i));
					
					rotaFinal = teste.menorRotaDestino(rotasIniciais);
					
					System.out.println(rotaFinal.getOrigem() + " " +rotaFinal.getDestino() + " "+ rotaFinal.getDistancia());
				}
				
			}
		}
		
		Double somaKm;
		
		somaKm = rotaFinal.getDistancia();
		
		String rotas;
		
		rotas = rotaFinal.getOrigem() +", "+ rotaFinal.getDestino();
		
		while (!rotaFinal.getDestino().equals(destino)) {
			
			for (int i = 0; i < listaRotas.size(); i++) {
				
				if (rotaFinal.getDestino().equals(listaRotas.get(i).getOrigem()) && destino.equals(rotaFinal.getDestino())) {
						
					rotasDestinos.add(listaRotas.get(i));
					
					rotaFinal = teste.menorRotaDestino(rotasDestinos);
					
					System.out.println(rotaFinal.getOrigem() + " " +rotaFinal.getDestino() + " "+ rotaFinal.getDistancia());
					
					somaKm = rotaFinal.getDistancia() + somaKm;
					
					if (!rotas.contains(rotaFinal.getDestino())) {
						rotas = rotas +", "+rotaFinal.getDestino();
					}
					
				}else if(rotaFinal.getDestino().equals(listaRotas.get(i).getOrigem())){
					
					rotasDestinos.add(listaRotas.get(i));
					
					for (int j = 0; j < rotasDestinos.size(); j++) {
						
						if (rotasDestinos.get(j).getDestino().equals(destino)) {
							
							rotaFinal = rotasDestinos.get(j);
							
							System.out.println(rotaFinal.getOrigem() + " " +rotaFinal.getDestino() + " "+ rotaFinal.getDistancia());
							
							somaKm = rotaFinal.getDistancia() + somaKm;
							
							if (!rotas.contains(rotaFinal.getDestino())) {
								rotas = rotas +", "+rotaFinal.getDestino();
							}
							
							rotasDestinos.clear();
						}
					}
					if (rotasDestinos.size() > 0) {
						
						rotaFinal = teste.menorRotaDestino(rotasDestinos);
						
						somaKm = rotaFinal.getDistancia() + somaKm;
						
						if (!rotas.contains(rotaFinal.getDestino())) {
							rotas = rotas +", "+rotaFinal.getDestino();
						}
						
						System.out.println(rotaFinal.getOrigem() + " " +rotaFinal.getDestino() + " "+ rotaFinal.getDistancia());
					}
					
					if (rotaControle != null 
							&& rotaControle.getDestino() == rotaFinal.getDestino() 
								&& rotaControle.getDistancia() == rotaFinal.getDistancia() 
									&& rotaControle.getOrigem() == rotaFinal.getOrigem()) {
						
						rotaFinal.setDestino(destino);
						rotas = "Não foi possivel estabelecer uma rota";
						autonomia = 0.00;
						somaKm = 0.00;
						autonomia = 0.00;
						break;
					}
				}
				if (rotaFinal.getDestino().equals(destino)) {
					break;
				}
				
				rotaControle = rotaFinal;
			}
		}
		
		System.out.println("KM Total " + somaKm);
		
		autonomia = somaKm/autonomia;
		
		autonomia = valorLitro*autonomia;
		
		System.out.println("Rota utilizada " + rotas);
		
		System.out.println("Custo " + autonomia);
		
	}

	public Rota menorRotaDestino(List<Rota> listaRotasDestinos){
		
		Rota menor = new Rota();
		
		for (Rota rota : listaRotasDestinos) {
			
			if (menor.getDistancia() == null) {
				
				menor = rota;
				
			}else {
				
				if (Double.compare(menor.getDistancia(), rota.getDistancia()) == 1 || Double.compare(menor.getDistancia(), rota.getDistancia()) == 0) {
					
					return rota;
					
				}else{
					
					return menor;
				}
			}
		}
		
		return menor;
	}
        
}

